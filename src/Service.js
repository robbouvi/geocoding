import axios from "axios";

let baseGeoCodeURL =
  "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyD0AYIDthpuhGN3NoJJdVQ8jB1_GF2WAA8";

export default {
  getLatLon(address, appkey) {
    if (address) {
      return axios.get(baseGeoCodeURL + "&address=" + address);
    }
  },
};
